from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from .forms import *
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from services.models import *
from .models import *
from django.views.generic import *
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist


@login_required
def home_view(request):
    my_recent_complaints = Complaint.objects.filter(
        user=request.user).all()[:10]
    my_complaints = Complaint.objects.filter(user=request.user).count()

    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "User is created successfully.")
    else:
        form = NewUserForm()

    total_vets = Vet.objects.count()
    total_users = User.objects.count()
    total_complaints = Complaint.objects.count()
    users = User.objects.exclude(username=request.user.username)
    profiles = Profile.objects.all()
    try:
        profile_id = Profile.objects.get(user=request.user).id
    except:
        profile_id = Profile.objects.count() + 1
    try:
        user_profile = Profile.objects.get(user=request.user)
    except:
        user_profile = None

    return render(request, template_name='base.html', context={
        'my_recent_complaints': my_recent_complaints,
        'my_complaints': my_complaints,
        'total_vets': total_vets,
        'total_users': total_users,
        'total_complaints': total_complaints,
        'users': users,
        'profiles': profiles,
        'user_profile': user_profile,
        'form': form,
        'id': profile_id})


@login_required
def profile_view(request):
    context = {}
    try:
        obj = get_object_or_404(Profile, user=request.user)

    except:
        Profile.objects.create(user=request.user, name="", mobile_no="")
        obj = get_object_or_404(Profile, user=request.user)

    profileform = ProfileForm(request.POST or None, instance=obj)
    if profileform.is_valid():
        form = profileform.save(commit=False)
        form.image = request.FILES.get('image')
        form.save()
        messages.success(request, 'Profile updated successfully')
        # return redirect('user-profile-update', id=id)
    context["profile_update_form"] = profileform
    context["profile"] = Profile.objects.get(user=request.user)
    return render(request, "accounts/profile.html", context)


@login_required
def profile_update_view(request, id):
    context = {}
    if id:
        try:
            obj = get_object_or_404(Profile, user_id=id)

        except:
            Profile.objects.create(user_id=id, name="", mobile_no="")
            obj = get_object_or_404(Profile, user_id=id)
    else:
        obj = Profile(user=request.user)
    profileform = ProfileForm(request.POST or None,  instance=obj)
    if profileform.is_valid():
        form = profileform.save(commit=False)
        form.image = request.FILES.get('image')
        form.save()
        messages.success(request, 'Profile updated successfully')
        return redirect('user-profile-update', id=id)
    context["profile_update_form"] = profileform
    context["profile"] = Profile.objects.get(user_id=id)
    return render(request, "accounts/profile-admin-view.html", context)


@login_required
def profile_delete_view(request, id):
    user = User.objects.get(id=id)
    if user:
        user.delete()
        messages.success(request, 'User deleted successfully')
    return redirect('home')


def register_view(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("login")
        else:
            messages.error(
                request, form.errors)
    form = NewUserForm()
    return render(request=request, template_name="accounts/register.html", context={"register_form": form})


def login_view(request):
    if request.method == "POST":
        form = NewAuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(
                    request, f"You are now logged in as {username}.")
                return redirect("home")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, form.errors)
    form = NewAuthenticationForm()
    return render(request=request, template_name="accounts/login.html", context={"login_form": form})


@login_required
def logout_view(request):
    logout(request)
    messages.info(request, f"You are successfully logged out.")
    return redirect('login')
