from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user
from .models import *

class NewUserForm(UserCreationForm):
	username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Username'}))
	email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Email Address'}))
	password1 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Password'}))
	password2 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Confirm Password'}))

	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")

	def save(self, commit=True):
		user = super(NewUserForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user
	
class NewAuthenticationForm(AuthenticationForm):
	username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Username'}))
	password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Password'}))

	class Meta:
		model = User
		fields = "__all__"

class ProfileForm(forms.ModelForm):
	image = forms.ImageField(
        widget = forms.FileInput(attrs={'name': 'image'}))
	class Meta:
		model = Profile
		exclude = ('user',)
