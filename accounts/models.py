from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.webp', upload_to='profile_pics')
    name = models.CharField(max_length = 200)
    mobile_no = models.CharField(max_length = 10, unique=True)

    def __str__(self):
        return f'{self.user.username} Profile'