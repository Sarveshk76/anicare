from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', home_view, name='home'),
    path('account/change-password', auth_views.PasswordChangeView.as_view(
            template_name='accounts/change-password.html',
            success_url = '/'), name='change-password'),
    path('account/profile', profile_view, name="user-profile"),
    path('account/profile/<int:id>', profile_update_view, name="user-profile-update"),
    path('account/profile/<int:id>/delete', profile_delete_view, name="user-profile-delete"),
    path('register/', register_view, name='register'),
    path('login/', login_view ,name='login'),
    path('logout/',logout_view ,name="logout")
]