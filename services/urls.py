from django.urls import path
from .views import *

urlpatterns = [
    path('vets/', VetCreateView.as_view() ,name="vet-list"),
    path('vets/<int:pk>', VetUpdateView.as_view() ,name="vet-detail"),
    path('vets/<int:pk>/delete', VetDeleteView, name="vet-delete"),

    path('complaints/', ComplaintCreateView.as_view() ,name="complaint-list"),
    path('complaints/<int:pk>', ComplaintUpdateView.as_view() ,name="complaint-detail"),
    path('complaints/<int:pk>/delete', ComplaintDeleteView, name="complaint-delete")
]