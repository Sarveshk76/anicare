from django import forms
from .models import *
from django.contrib.auth import get_user
from django.contrib.auth.models import User
from .models import *

class VetForm(forms.ModelForm):
    class Meta:
        model = Vet
        fields = '__all__'


class ComplaintForm(forms.ModelForm):
    class Meta:
        model = Complaint
        exclude = ('user','is_resolved')

    def save(self, commit=True):
         form = super().save(commit=False)
         if not form.pk:
             form.user = get_user(self.request)
         if commit:
             form.save()
             self.save_m2m()
         return form