from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import json


class VetCreateView(CreateView):
    model = Vet
    fields = '__all__'
    template_name = "services/vetlistview.html"
    success_url = reverse_lazy('vet-list')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VetCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        messages.success(self.request, "Vet is created successfully.")
        return super(VetCreateView,self).form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['vet_list'] = Vet.objects.all()
        except:
            context['vet-list'] = None
        return context


class VetUpdateView(UpdateView):
    model = Vet
    fields = '__all__'
    template_name = "services/vetdetailview.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(VetUpdateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("vet-detail", kwargs={"pk": pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        context['object'] = Vet.objects.get(id=self.kwargs['pk'])
        return context

@login_required
def VetDeleteView(request, pk):
    print("PK: ",pk)
    vet = Vet.objects.get(id=pk)
    vet.delete()
    return redirect(reverse('vet-list'))


class ComplaintCreateView(CreateView):
    model = Complaint
    form_class = ComplaintForm
    template_name = "services/complaintlistview.html"
    success_url = reverse_lazy('complaint-list')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ComplaintCreateView, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.request = self.request
        return form

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.updated_by = self.request.user
        messages.success(self.request, "Complaint is created successfully.")
        return super(ComplaintCreateView,self).form_valid(form)
    
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['complaint_list'] = Complaint.objects.all()
        context['user_complaint_list'] = Complaint.objects.filter(user=self.request.user).all()
        return context


class ComplaintUpdateView(UpdateView):
    model = Complaint
    form_class = ComplaintForm
    template_name = "services/complaintdetailview.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ComplaintUpdateView, self).dispatch(*args, **kwargs)
    
    def get_success_url(self):
           pk = self.kwargs["pk"]
           return reverse("complaint-detail", kwargs={"pk": pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['id'] = self.kwargs['pk']
        c_obj = Complaint.objects.get(id=self.kwargs['pk'])
        context['object'] = c_obj
        res_obj = self.request.POST.get('is_resolved')
        if res_obj == 'True':
            c_obj.is_resolved = True
        elif res_obj == 'False':
            c_obj.is_resolved = False
        c_obj.save()
        return context

@login_required
def ComplaintDeleteView(request, pk):
    complaint = Complaint.objects.get(id=pk)
    complaint.delete()
    return redirect(reverse('complaint-list'))

