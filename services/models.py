from django.conf import settings
from django.utils import timezone
from django.db import models
import os

DES = (
        ("---", "---"),
        ("Veterinary Specialist","Veterinary Specialist"),
       ("Companion Animal Veterinarian","Companion Animal Veterinarian"),
       ("Food-Animal Veterinarian", "Food-Animal Veterinarian"),
       ("Food Safety and Research Veterinarian", "Food Safety and Research Veterinarian"),
       ("Research Veterinarian","Research Veterinarian"))

class Vet(models.Model):
    name = models.CharField(max_length = 200, unique=True)
    designation = models.CharField(
        max_length = 50,
        choices = DES,
        default = '1'
        )
    address = models.TextField()
    mobile_no = models.CharField(max_length = 10, db_column='contact_info')
    website = models.URLField(max_length = 200)

    def __str__(self):
        return self.name


class Complaint(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        editable=False,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length = 100)
    description = models.TextField()
    images = models.ImageField(default='default.jpg', upload_to='images/')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_resolved = models.BooleanField(default=False)

    def __str__(self):
        return self.title
