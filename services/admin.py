from django.contrib import admin
from .models import *

admin.site.register(Vet)
admin.site.register(Complaint)
